
# Inventory App
Inventory App will allow a store to keep track of its inventory of products.  
The app will need to store information about the product and allow the user  
to track sales and shipments and make it easy for the user to order more from the listed supplier.

## Purpose
This app is not on Google Play Store and has been developed for learning and experience sharing purposes.  
It purely rely on offline storage mode by storing all the data in a local SQLite DB and work with Loaders and Cursor adapter for loading efficiency . 

## Features
Inventory app includes  CRUD operations for a on asynchronous processing for data from SQLite  
data using Loaders to fetch the data smoothly and notify when the content changes.  
It also cached product box session into a Singleton to use it through the app. 

## Android Libraries used
This project demonstrates the use of some useful android libraries such as:   
  -  **Butterknife** for injection on UI.   
  -  **Lottie** for  animation.
 
## Setup

### Clone the Repository

As usual, you get started by cloning the project to your local machine:
```
$ git clone https://audreyange93@bitbucket.org/audreyteam/inventory-app.git
```
You can also create a new project from Android Studio by importing this project from git using the above git repository url.

### Prerequisites

This project uses JDK 7  and  has been build using android SDK 27. 
### Screenshots
![screenshot1](https://drive.google.com/uc?export=download&id=1lgu5y5b5vUwhmeavTm29lDbLj_JWn5Tc)

## Screencast
Here a video showing a demo of the app :
Inventory App Video.

## Authors

* **Audrey Maumoin**      
Android Developer

## Acknowledgments
I hope that you will have a good time navigating through the project.   
Also feel free to comment or ask if you want to know more or have some suggestions.   
**Happy Coding** :thumbsup: