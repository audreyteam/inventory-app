package com.example.android.inventoryapp.fragment;

import static com.example.android.inventoryapp.fragment.CategoryCatalogFragment.EXTRA_CATEGORY_POSITION;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import butterknife.BindView;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.activity.ProductDetailsActivity;
import com.example.android.inventoryapp.adapter.ProductCursorAdapter;
import com.example.android.inventoryapp.adapter.ProductCursorAdapter.OnProductClickListener;
import com.example.android.inventoryapp.data.ProductContract.ProductEntry;

public class ProductCatalogFragment extends CatalogFragment implements LoaderCallbacks<Cursor>,
    OnProductClickListener, FilterQueryProvider {

  public static final int PRODUCT_DETAILS_REQUEST_CODE = 1;
  public static final int CATEGORIES_LOADER_ID = 1;
  public static final int PRODUCTS_LOADER_ID = 0;
  private static final int CATEGORY_DEFAULT_POSITION = -1;
  private static final String SEARCH_TEXT_STATE = "search_state";

  @BindView(R.id.empty_view)
  View emptyView;

  private SearchView searchView;
  private ProductCursorAdapter adapter;
  private boolean isFilteredByCategory = false;
  private int categoryPosition;
  private String searchText;
  private int clickedProductPosition = -1;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    // Retrieves the search text from the saved state.
    if (savedInstanceState != null) {
      searchText = savedInstanceState.getString(SEARCH_TEXT_STATE);
    }
    categoryPosition = getActivity().getIntent().getIntExtra(EXTRA_CATEGORY_POSITION,
        CATEGORY_DEFAULT_POSITION);
    // Checks if the user needs to display the list of products depending on the category.
    isFilteredByCategory = categoryPosition != CATEGORY_DEFAULT_POSITION;
    if (isFilteredByCategory) {
      Resources res = getResources();
      String[] categoryNames = res.getStringArray(R.array.category_names);
      getActivity().setTitle(categoryNames[categoryPosition]);
      getActivity().getSupportLoaderManager().initLoader(CATEGORIES_LOADER_ID, null, this);
    } else {
      getActivity().getSupportLoaderManager().initLoader(PRODUCTS_LOADER_ID, null, this);
    }
    adapter = new ProductCursorAdapter(getActivity(), null);
    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
    recyclerView.setLayoutManager(mLayoutManager);
    adapter.setOnProductClick(this);
    adapter.setFilterQueryProvider(this);
    recyclerView.setAdapter(adapter);
    return view;
  }

  @Override
  public void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    // Saves the search text before the activity destroy.
    String searchText = searchView.getQuery().toString();
    outState.putString(SEARCH_TEXT_STATE, searchText);
  }

  @Override
  public int getLayoutRes() {
    return R.layout.fragment_product_list;
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    SearchManager searchManager =
        (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
    MenuItem searchItem = menu.findItem(R.id.search);
    searchView =
        (SearchView) searchItem.getActionView();
    searchView.setMaxWidth(Integer.MAX_VALUE);
    searchView.setSearchableInfo(
        searchManager.getSearchableInfo(getActivity().getComponentName()));
    // Avoid submitting query with empty text when calling invalidateOptionMenu.
    searchView.setQuery("", false);
    searchView.setOnQueryTextListener(new OnQueryTextListener() {
      @Override
      public boolean onQueryTextSubmit(String query) {
        adapter.getFilter().filter(query);
        return true;
      }

      @Override
      public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
      }
    });
    if (!TextUtils.isEmpty(searchText)) {
      searchItem.expandActionView();
      searchView.setQuery(searchText, true);
      searchView.clearFocus();
    }
  }

  @Override
  public void onPrepareOptionsMenu(Menu menu) {
    super.onPrepareOptionsMenu(menu);
    MenuItem menuItem = menu.findItem(R.id.search);
    // Hides the search menu when the list of products is filtered.
    menuItem.setVisible(!isFilteredByCategory);
  }

  @NonNull
  @Override
  public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
    String[] projection = {ProductEntry._ID,
        ProductEntry.COLUMN_PRODUCT_NAME,
        ProductEntry.COLUMN_PRODUCT_DESCRIPTION,
        ProductEntry.COLUMN_PRODUCT_PRICE,
        ProductEntry.COLUMN_PRODUCT_QUANTITY,
        ProductEntry.COLUMN_IMAGE,
        ProductEntry.COLUMN_CATEGORY};

    switch (id) {
      case PRODUCTS_LOADER_ID:
        return new CursorLoader(getActivity(), ProductEntry.CONTENT_URI, projection, null, null,
            ProductEntry.SORT_BY_NAME);
      case CATEGORIES_LOADER_ID:
      default:
        String selection = ProductEntry.COLUMN_CATEGORY + "=?";
        String[] selectionArgs = new String[]{String.valueOf(categoryPosition)};
        return new CursorLoader(getActivity(), ProductEntry.CONTENT_URI, projection, selection,
            selectionArgs,
            ProductEntry.SORT_BY_NAME);
    }
  }

  @Override
  public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
    adapter.swapCursor(data);
    if (data.getCount() == 0) {
      emptyView.setVisibility(View.VISIBLE);
      recyclerView.setVisibility(View.GONE);
    } else {
      emptyView.setVisibility(View.GONE);
      recyclerView.setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void onLoaderReset(@NonNull Loader<Cursor> loader) {
    adapter.swapCursor(null);
  }

  @Override
  public void onProductClick(long productId, int position) {
    clickedProductPosition = position;
    Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
    Uri rowUri = ContentUris.withAppendedId(ProductEntry.CONTENT_URI, productId);
    intent.setData(rowUri);
    startActivityForResult(intent, PRODUCT_DETAILS_REQUEST_CODE);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == PRODUCT_DETAILS_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
      adapter.notifyItemChanged(clickedProductPosition);
    } else if (requestCode == STORE_BOX_REQUEST_CODE || requestCode == SETTINGS_REQUEST_CODE) {
      adapter.notifyDataSetChanged();
    }
  }

  @Override
  public void onBoxIconClick(long productId, int position) {
    boolean isBoxed = boxProduct(productId);
    if (isBoxed) {
      adapter.notifyItemChanged(position);
    }
  }

  @Override
  public Cursor runQuery(CharSequence constraint) {
    String wordToQuery = constraint.toString();

    String[] projection = {ProductEntry._ID,
        ProductEntry.COLUMN_PRODUCT_NAME,
        ProductEntry.COLUMN_PRODUCT_DESCRIPTION,
        ProductEntry.COLUMN_PRODUCT_PRICE,
        ProductEntry.COLUMN_PRODUCT_QUANTITY,
        ProductEntry.COLUMN_IMAGE,
        ProductEntry.COLUMN_CATEGORY};
    String selection = ProductEntry.COLUMN_PRODUCT_NAME + " LIKE ?";
    String[] selectionArgs = new String[]{"%" + wordToQuery + "%"};
    Cursor cursor = getActivity().getContentResolver()
        .query(ProductEntry.CONTENT_URI, projection, selection, selectionArgs,
            ProductEntry.SORT_BY_NAME);
    return cursor;
  }
}
