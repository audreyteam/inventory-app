package com.example.android.inventoryapp.adapter;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.ImageView;
import com.example.android.inventoryapp.utils.BitmapUtils;
import java.lang.ref.WeakReference;

/**
 * Task that loads a disk image using the path {@link Uri} in the background.
 */
public class ProductImageTask extends AsyncTask<String, Void, Bitmap> {

  private final WeakReference<ImageView> imageViewRef;

  ProductImageTask(ImageView imageView) {
    this.imageViewRef = new WeakReference<>(imageView);
  }

  @Override
  protected Bitmap doInBackground(String... uris) {
    String uri = uris[0];
    return BitmapUtils.getBitmapFromUri(imageViewRef.get(), Uri.parse(uri));
  }

  @Override
  protected void onPostExecute(Bitmap bitmap) {
    super.onPostExecute(bitmap);
    ImageView imageView = imageViewRef.get();
    if (imageView != null) {
      imageView.setImageBitmap(bitmap);
    }
  }
}
