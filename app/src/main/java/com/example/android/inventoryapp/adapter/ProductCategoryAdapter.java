package com.example.android.inventoryapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.adapter.ProductCategoryAdapter.CategoryViewHolder;
import com.example.android.inventoryapp.model.ProductCategory;
import java.util.List;

public class ProductCategoryAdapter extends Adapter<CategoryViewHolder> {

  private List<ProductCategory> categoryList;
  private OnCategoryClickListener listener;

  public ProductCategoryAdapter(List<ProductCategory> categoryList) {
    this.categoryList = categoryList;
  }

  @NonNull
  @Override
  public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.category_list_item, parent, false);
    CategoryViewHolder viewHolder = new CategoryViewHolder(view);
    return viewHolder;
  }

  @Override
  public void onBindViewHolder(@NonNull CategoryViewHolder holder, final int position) {
    ProductCategory category = categoryList.get(position);
    holder.categoryImage.setImageResource(category.getImageResId());
    holder.categoryName.setText(category.getName());
    holder.itemLayout.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        listener.onCategoryClick(position);
      }
    });
  }

  @Override
  public int getItemCount() {
    return categoryList.size();
  }

  public void setOnCategoryClick(OnCategoryClickListener listener) {
    this.listener = listener;
  }

  /**
   * Listener for Product category click event.
   */
  public interface OnCategoryClickListener {

    void onCategoryClick(int position);
  }

  static class CategoryViewHolder extends ViewHolder {

    @BindView(R.id.category_image)
    ImageView categoryImage;
    @BindView(R.id.category_name)
    TextView categoryName;
    @BindView(R.id.item_layout)
    LinearLayout itemLayout;

    CategoryViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
