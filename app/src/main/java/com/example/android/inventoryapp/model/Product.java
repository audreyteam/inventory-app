package com.example.android.inventoryapp.model;

import android.database.Cursor;
import com.example.android.inventoryapp.data.ProductContract.ProductEntry;

public final class Product {

  private final long id;
  private final String name;
  private final String description;
  private final float price;
  private final int quantity;
  private final int category;
  private final String photoUri;
  private final String supplierName;
  private final String supplierNumber;
  private final String supplierAddress;

  private Product(ProductBuilder builder) {
    id = builder.id;
    name = builder.name;
    description = builder.description;
    price = builder.price;
    quantity = builder.quantity;
    category = builder.category;
    photoUri = builder.photoUri;
    supplierName = builder.supplierName;
    supplierNumber = builder.supplierNumber;
    supplierAddress = builder.supplierAddress;
  }

  public static Product fromCursor(Cursor cursor) {
    int productNameColumnIndex = cursor.getColumnIndex(ProductEntry.COLUMN_PRODUCT_NAME);
    int descriptionColumnIndex = cursor.getColumnIndex(ProductEntry.COLUMN_PRODUCT_DESCRIPTION);
    int priceColumnIndex = cursor.getColumnIndex(ProductEntry.COLUMN_PRODUCT_PRICE);
    int quantityColumnIndex = cursor.getColumnIndex(ProductEntry.COLUMN_PRODUCT_QUANTITY);
    int categoryColumnIndex = cursor.getColumnIndex(ProductEntry.COLUMN_CATEGORY);
    int imageColumnIndex = cursor.getColumnIndex(ProductEntry.COLUMN_IMAGE);
    int supplierNameColumnIndex = cursor.getColumnIndex(ProductEntry.COLUMN_SUPPLIER_NAME);
    int supplierPhoneColumnIndex = cursor.getColumnIndex(ProductEntry.COLUMN_SUPPLIER_PHONE);
    int supplierAddressColumnIndex = cursor.getColumnIndex(ProductEntry.COLUMN_SUPPLIER_ADDRESS);
    int idColumnIndex = cursor.getColumnIndex(ProductEntry._ID);

    String productName = cursor.getString(productNameColumnIndex);
    float price = cursor.getFloat(priceColumnIndex);
    int quantity = cursor.getInt(quantityColumnIndex);
    int category = cursor.getInt(categoryColumnIndex);
    String imageUri = cursor.getString(imageColumnIndex);
    long id = cursor.getLong(idColumnIndex);
    ProductBuilder builder = new ProductBuilder();
    builder.setId(id)
        .setName(productName)
        .setPrice(price)
        .setQuantity(quantity)
        .setPhotoUri(imageUri)
        .setCategory(category);
    if (descriptionColumnIndex > -1) {
      String description = cursor.getString(descriptionColumnIndex);
      builder.setDescription(description);
    }
    if (supplierNameColumnIndex > -1) {
      String supplierName = cursor.getString(supplierNameColumnIndex);
      builder.setSupplierName(supplierName);
    }
    if (supplierPhoneColumnIndex > -1) {
      String supplierPhone = cursor.getString(supplierPhoneColumnIndex);
      builder.setSupplierNumber(supplierPhone);
    }
    if (supplierAddressColumnIndex > -1) {
      String supplierAddress = cursor.getString(supplierAddressColumnIndex);
      builder.setSupplierAddress(supplierAddress);
    }
    return builder.create();
  }

  public String getName() {
    return name;
  }

  public float getPrice() {
    return price;
  }

  public int getQuantity() {
    return quantity;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public String getSupplierNumber() {
    return supplierNumber;
  }

  public String getDescription() {
    return description;
  }

  public int getCategory() {
    return category;
  }

  public String getPhotoUri() {
    return photoUri;
  }

  public String getSupplierAddress() {
    return supplierAddress;
  }

  public long getId() {
    return id;
  }

  public static class ProductBuilder {

    private long id;
    private String name;
    private String description;
    private float price;
    private int quantity;
    private int category;
    private String photoUri;
    private String supplierName;
    private String supplierNumber;
    private String supplierAddress;

    public ProductBuilder setName(String name) {
      this.name = name;
      return this;
    }

    public ProductBuilder setPrice(float price) {
      this.price = price;
      return this;
    }

    public ProductBuilder setQuantity(int quantity) {
      this.quantity = quantity;
      return this;
    }

    public ProductBuilder setSupplierName(String supplierName) {
      this.supplierName = supplierName;
      return this;
    }

    public ProductBuilder setSupplierNumber(String supplierNumber) {
      this.supplierNumber = supplierNumber;
      return this;
    }

    public ProductBuilder setDescription(String description) {
      this.description = description;
      return this;
    }

    public ProductBuilder setCategory(int category) {
      this.category = category;
      return this;
    }

    public ProductBuilder setPhotoUri(String photoUri) {
      this.photoUri = photoUri;
      return this;
    }

    public ProductBuilder setSupplierAddress(String supplierAddress) {
      this.supplierAddress = supplierAddress;
      return this;
    }

    public ProductBuilder setId(long id) {
      this.id = id;
      return this;
    }

    public Product create() {
      return new Product(this);
    }
  }
}
