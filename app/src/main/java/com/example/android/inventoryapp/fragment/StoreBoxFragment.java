package com.example.android.inventoryapp.fragment;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.activity.CashRegisterActivity;
import com.example.android.inventoryapp.adapter.ItemBoxCursorAdapter;
import com.example.android.inventoryapp.adapter.ItemBoxCursorAdapter.OnItemBoxChangeListener;
import com.example.android.inventoryapp.data.ProductContract.ProductEntry;
import com.example.android.inventoryapp.data.StoreBox;
import com.example.android.inventoryapp.model.Product;
import com.example.android.inventoryapp.utils.NumberFormatter;
import com.example.android.inventoryapp.utils.PreferencesUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Set;

public class StoreBoxFragment extends Fragment implements LoaderCallbacks<Cursor>,
    OnItemBoxChangeListener {

  public static final int ITEM_BOX_LOADER_ID = 0;
  private static final int CURSOR_INITIAL_POSITION = -1;

  @BindView(R.id.store_box_list)
  RecyclerView storeBoxList;
  @BindView(R.id.total_price)
  TextView totalPriceView;
  @BindView(R.id.items_container)
  RelativeLayout itemsContainer;
  @BindView(R.id.empty_view)
  TextView emptyView;
  @BindView(R.id.sell_button)
  Button sellButton;

  private Unbinder unbinder;
  private ItemBoxCursorAdapter adapter;
  private ArrayList<Integer> quantityList;
  private ArrayList<Float> priceList;
  private float totalPrice;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View view = inflater.inflate(R.layout.fragment_store_cart, container, false);
    unbinder = ButterKnife.bind(this, view);
    adapter = new ItemBoxCursorAdapter(getActivity(), null, this);
    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
    storeBoxList.setLayoutManager(mLayoutManager);
    storeBoxList.setAdapter(adapter);
    // when the store box is empty, show the empty view or else display items in the box.
    if (StoreBox.getInstance().getSize() != 0) {
      getActivity().getSupportLoaderManager().initLoader(ITEM_BOX_LOADER_ID, null, this);
    } else {
      itemsContainer.setVisibility(View.GONE);
      emptyView.setVisibility(View.VISIBLE);
    }
    return view;
  }

  @Override
  public void onProductQuantityChanged() {
    totalPrice = calculateTotalPrice();
    StringBuilder totalPriceText = new StringBuilder(getString(R.string.total_price_text));
    totalPriceText.append(NumberFormatter.formatPrice(getActivity(), totalPrice));
    totalPriceView.setText(totalPriceText);
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @Override
  public void onStop() {
    super.onStop();
    getActivity().setResult(Activity.RESULT_OK);
  }

  @OnClick(R.id.sell_button)
  public void onSellButtonClicked() {
    new UpdateProductTask(StoreBoxFragment.this).execute(StoreBox.getInstance().getItemsIds());
  }

  @Override
  public void onShowDeleteDialog(final long productId) {
    Builder builder = new Builder(getActivity());
    builder.setMessage(R.string.delete_box_item_dialog_msg);
    builder.setPositiveButton(R.string.delete, new OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        StoreBox.getInstance().removeItem(productId);
        getActivity().getSupportLoaderManager()
            .restartLoader(ITEM_BOX_LOADER_ID, null, StoreBoxFragment.this);
      }
    });
    builder.setNegativeButton(R.string.cancel, new OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        // Dismiss the dialog when the user clicked the "Cancel" button.
        if (dialog != null) {
          dialog.dismiss();
        }
      }
    });
    // Create and show the AlertDialog
    AlertDialog alertDialog = builder.create();
    alertDialog.show();
  }

  /**
   * Gets the selection used to make the query.
   */
  private String generateSelection() {
    StoreBox box = StoreBox.getInstance();
    StringBuilder builder = new StringBuilder(" IN (?");
    for (int i = 0; i < box.getSize(); i++) {
      builder.append(",?");
    }
    builder.append(")");
    return ProductEntry._ID + builder.toString();
  }

  /**
   * Gets the selection args used to make the query.
   */
  private String[] generateSelectionArgs() {
    StoreBox box = StoreBox.getInstance();
    ArrayList<String> list = new ArrayList<>();
    for (long itemId : box.getItemsIds()) {
      list.add(String.valueOf(itemId));
    }
    return list.toArray(new String[box.getSize()]);
  }

  /**
   * Calculates the price of all items in the box.
   */
  private float calculateTotalPrice() {
    if (priceList.size() != StoreBox.getInstance().getSize()) {
      return 0;
    }
    int i = 0;
    int sum = 0;
    for (int value : StoreBox.getInstance().itemsQuantities()) {
      sum += value * priceList.get(i);
      i++;
    }
    return sum;
  }

  @NonNull
  @Override
  public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
    String[] projection = {ProductEntry._ID,
        ProductEntry.COLUMN_PRODUCT_NAME,
        ProductEntry.COLUMN_PRODUCT_DESCRIPTION,
        ProductEntry.COLUMN_PRODUCT_PRICE,
        ProductEntry.COLUMN_PRODUCT_QUANTITY,
        ProductEntry.COLUMN_IMAGE,
        ProductEntry.COLUMN_CATEGORY};
    return new CursorLoader(getActivity(), ProductEntry.CONTENT_URI, projection,
        generateSelection(), generateSelectionArgs(),
        ProductEntry.SORT_BY_NAME);
  }

  @Override
  public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
    if (data.getCount() == 0) {
      itemsContainer.setVisibility(View.GONE);
      emptyView.setVisibility(View.VISIBLE);
    } else {
      data.moveToPosition(CURSOR_INITIAL_POSITION);
      itemsContainer.setVisibility(View.VISIBLE);
      emptyView.setVisibility(View.GONE);
      priceList = new ArrayList<>();
      quantityList = new ArrayList<>();
      while (data.moveToNext()) {
        Product product = Product.fromCursor(data);
        priceList.add(product.getPrice());
        quantityList.add(product.getQuantity());
      }
      onProductQuantityChanged();
    }
    adapter.swapCursor(data);
  }

  @Override
  public void onLoaderReset(@NonNull Loader<Cursor> loader) {
    adapter.swapCursor(null);
  }

  private static class UpdateProductTask extends AsyncTask<Set<Long>, Void, Void> {

    private WeakReference<StoreBoxFragment> fragmentRef;

    UpdateProductTask(StoreBoxFragment fragment) {
      this.fragmentRef = new WeakReference<>(fragment);
    }

    @Override
    protected Void doInBackground(Set<Long>... idsSet) {
      int i = 0;
      StoreBoxFragment fragment = fragmentRef.get();
      for (long id : idsSet[0]) {
        Uri uri = ContentUris.withAppendedId(ProductEntry.CONTENT_URI, id);
        ContentValues values = new ContentValues();
        int quantityUpdated =
            fragment.quantityList.get(i) - StoreBox.getInstance().getQuantityInBox(id);
        values.put(ProductEntry.COLUMN_PRODUCT_QUANTITY, quantityUpdated);
        fragment.getActivity().getContentResolver().update(uri, values, null, null);
        i++;
      }
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      if (fragmentRef.get() != null) {
        StoreBoxFragment fragment = fragmentRef.get();
        PreferencesUtils.putCash(fragment.getActivity(), fragment.totalPrice);
        Intent cashRegisterIntent = new Intent(fragment.getActivity(), CashRegisterActivity.class);
        fragment.startActivity(cashRegisterIntent);
        fragment.getActivity().finish();
        StoreBox.getInstance().clearItems();
      }
    }
  }
}
