package com.example.android.inventoryapp.model;

public final class BoxItem {

  private final long itemId;
  private final int quantityToSell;

  public BoxItem(long itemId, int quantityToSell) {
    this.itemId = itemId;
    this.quantityToSell = quantityToSell;
  }

  public long getItemId() {
    return itemId;
  }

  public int getQuantityToSell() {
    return quantityToSell;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BoxItem item = (BoxItem) o;
    return itemId == item.itemId;
  }

  @Override
  public int hashCode() {
    return (int)(itemId ^ (itemId >>> 32));
  }
}
