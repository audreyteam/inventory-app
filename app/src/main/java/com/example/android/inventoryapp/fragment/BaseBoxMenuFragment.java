package com.example.android.inventoryapp.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.activity.CatalogActivity.OnInvalidateOptionsMenuListener;
import com.example.android.inventoryapp.activity.StoreBoxActivity;
import com.example.android.inventoryapp.data.StoreBox;
import com.example.android.inventoryapp.model.BoxItem;

public abstract class BaseBoxMenuFragment extends Fragment implements
    OnInvalidateOptionsMenuListener {

  protected static final int STORE_BOX_REQUEST_CODE = 3;
  private static final int MIN_QUANTITY_IN_STOCK = 1;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override
  public void onStart() {
    super.onStart();
    onInvalidateOptionsMenu();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.box:
        Intent boxIntent = new Intent(getActivity(), StoreBoxActivity.class);
        startActivityForResult(boxIntent, STORE_BOX_REQUEST_CODE);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_box, menu);
    final MenuItem menuItem = menu.findItem(R.id.box);
    View view = menuItem.getActionView();
    TextView badgeTextView = view.findViewById(R.id.box_badge);
    view.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        onOptionsItemSelected(menuItem);
      }
    });
    setBoxBadge(badgeTextView);
  }

  /**
   * Sets the number of items added in the box into the badgeTextView.
   *
   * @param badgeTextView The {@link TextView} that shows the number of boxed items.
   */
  private void setBoxBadge(TextView badgeTextView) {
    int count = StoreBox.getInstance().getSize();
    if (count == 0) {
      badgeTextView.setVisibility(View.GONE);
    } else {
      badgeTextView.setVisibility(View.VISIBLE);
      badgeTextView.setText(String.valueOf(count));
    }
  }

  protected boolean boxProduct(long rowId) {
    StoreBox box = StoreBox.getInstance();
    int quantityInBox = box.getQuantityInBox(rowId);
    if (quantityInBox == 0) {
      BoxItem item = new BoxItem(rowId, MIN_QUANTITY_IN_STOCK);
      box.addItem(item);
      Toast.makeText(getActivity(), getString(R.string.add_to_box_successful), Toast.LENGTH_SHORT)
          .show();
      onInvalidateOptionsMenu();
      return true;
    } else {
      showBoxDialog();
      return false;
    }
  }

  private void showBoxDialog() {
    Builder builder = new Builder(getActivity());
    builder.setMessage(R.string.box_dialog_msg);
    builder.setPositiveButton(R.string.go_to_box, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        Intent boxIntent = new Intent(getActivity(), StoreBoxActivity.class);
        startActivity(boxIntent);
      }
    });
    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        // Dismiss the dialog when the user clicked the "Cancel" button.
        if (dialog != null) {
          dialog.dismiss();
        }
      }
    });
    // Create and show the AlertDialog
    AlertDialog alertDialog = builder.create();
    alertDialog.show();
  }

  @Override
  public void onInvalidateOptionsMenu() {
    getActivity().invalidateOptionsMenu();
  }
}
