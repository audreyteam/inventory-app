package com.example.android.inventoryapp.data;

import static com.example.android.inventoryapp.data.ProductContract.CONTENT_AUTHORITY;
import static com.example.android.inventoryapp.data.ProductContract.PATH_PRODUCTS;
import static com.example.android.inventoryapp.data.ProductContract.ProductEntry.COLUMN_PRODUCT_DESCRIPTION;
import static com.example.android.inventoryapp.data.ProductContract.ProductEntry.COLUMN_PRODUCT_NAME;
import static com.example.android.inventoryapp.data.ProductContract.ProductEntry.COLUMN_PRODUCT_PRICE;
import static com.example.android.inventoryapp.data.ProductContract.ProductEntry.COLUMN_PRODUCT_QUANTITY;
import static com.example.android.inventoryapp.data.ProductContract.ProductEntry.COLUMN_SUPPLIER_NAME;
import static com.example.android.inventoryapp.data.ProductContract.ProductEntry.COLUMN_SUPPLIER_PHONE;
import static com.example.android.inventoryapp.data.ProductContract.ProductEntry.CONTENT_ITEM_TYPE;
import static com.example.android.inventoryapp.data.ProductContract.ProductEntry.CONTENT_LIST_TYPE;
import static com.example.android.inventoryapp.data.ProductContract.ProductEntry.TABLE_NAME;
import static com.example.android.inventoryapp.data.ProductContract.ProductEntry._ID;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class ProductProvider extends ContentProvider {

  private static final int PRODUCTS = 1000;
  private static final int PRODUCT_ID = 1001;
  private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

  // Static initializer. This is run the first time anything is called from this class.
  static {
    sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_PRODUCTS, PRODUCTS);
    sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_PRODUCTS + "/#", PRODUCT_ID);
  }

  private ProductDbHelper productDbHelper;

  @Override
  public boolean onCreate() {
    productDbHelper = new ProductDbHelper(getContext());
    return true;
  }

  @Nullable
  @Override
  public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection,
      @Nullable String[] selectionArgs, @Nullable String sortOrder) {
    SQLiteDatabase database = productDbHelper.getReadableDatabase();
    Cursor cursor;
    int match = sUriMatcher.match(uri);
    switch (match) {
      case PRODUCTS:
        cursor = database
            .query(TABLE_NAME, projection, selection, selectionArgs, null, null,
                sortOrder);
        break;
      case PRODUCT_ID:
        selection = _ID + "=?";
        selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
        cursor = database.query(TABLE_NAME, projection, selection, selectionArgs,
            null, null, sortOrder);
        break;
      default:
        throw new IllegalArgumentException("Cannot query unknown URI " + uri);
    }
    cursor.setNotificationUri(getContext().getContentResolver(), uri);
    return cursor;
  }

  @Nullable
  @Override
  public String getType(@NonNull Uri uri) {
    final int match = sUriMatcher.match(uri);
    switch (match) {
      case PRODUCTS:
        return CONTENT_LIST_TYPE;
      case PRODUCT_ID:
        return CONTENT_ITEM_TYPE;
      default:
        throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
    }
  }

  @Nullable
  @Override
  public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
    final int match = sUriMatcher.match(uri);
    switch (match) {
      case PRODUCTS:
        return insertPet(uri, values);
      default:
        throw new IllegalArgumentException("Insertion is not supported for " + uri);
    }
  }

  private Uri insertPet(Uri uri, ContentValues values) {
    String name = values.getAsString(COLUMN_PRODUCT_NAME);
    if (TextUtils.isEmpty(name)) {
      throw new IllegalArgumentException("Product requires a name");
    }
    String description = values.getAsString(COLUMN_PRODUCT_DESCRIPTION);
    if (TextUtils.isEmpty(description)) {
      throw new IllegalArgumentException("Product requires a description");
    }
    Float price = values.getAsFloat(COLUMN_PRODUCT_PRICE);
    if (price != null && price <= 0) {
      throw new IllegalArgumentException("Product requires a valid price");
    }
    int quantity = values.getAsInteger(COLUMN_PRODUCT_QUANTITY);
    if (quantity <= 0) {
      throw new IllegalArgumentException("Product requires a valid quantity");
    }
    String supplierName = values.getAsString(COLUMN_SUPPLIER_NAME);
    if (TextUtils.isEmpty(supplierName)) {
      throw new IllegalArgumentException("Supplier name required");
    }
    String supplierPhone = values.getAsString(COLUMN_SUPPLIER_PHONE);
    if (TextUtils.isEmpty(supplierPhone)) {
      throw new IllegalArgumentException("Supplier phone required");
    }
    SQLiteDatabase database = productDbHelper.getWritableDatabase();
    // Insert the new product
    long id = database.insert(TABLE_NAME, null, values);
    // If the ID is -1, then the insertion failed.
    if (id == -1) {
      return null;
    }
    getContext().getContentResolver().notifyChange(uri, null);
    // Return the new URI with the ID (of the newly inserted row) appended at the end
    return ContentUris.withAppendedId(uri, id);
  }

  @Override
  public int delete(@NonNull Uri uri, @Nullable String selection,
      @Nullable String[] selectionArgs) {
    SQLiteDatabase database = productDbHelper.getWritableDatabase();
    final int match = sUriMatcher.match(uri);
    switch (match) {
      case PRODUCTS:
        // Delete all rows that match the selection and selection args
        return deleteProduct(database, uri, selection, selectionArgs);
      case PRODUCT_ID:
        // Delete a single row given by the ID in the URI
        selection = _ID + "=?";
        selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
        return deleteProduct(database, uri, selection, selectionArgs);
      default:
        throw new IllegalArgumentException("Deletion is not supported for " + uri);
    }
  }

  private int deleteProduct(SQLiteDatabase database, Uri uri, String selection,
      String[] selectionArgs) {
    int rowId = database.delete(TABLE_NAME, selection, selectionArgs);
    if (rowId != 0) {
      getContext().getContentResolver().notifyChange(uri, null);
    }
    return rowId;
  }

  @Override
  public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection,
      @Nullable String[] selectionArgs) {
    final int match = sUriMatcher.match(uri);
    switch (match) {
      case PRODUCTS:
        return updateProduct(uri, values, selection, selectionArgs);
      case PRODUCT_ID:
        selection = _ID + "=?";
        selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
        return updateProduct(uri, values, selection, selectionArgs);
      default:
        throw new IllegalArgumentException("Update is not supported for " + uri);
    }
  }

  private int updateProduct(Uri uri, ContentValues values, String selection,
      String[] selectionArgs) {
    if (values.size() == 0) {
      return 0;
    }
    if (values.containsKey(COLUMN_PRODUCT_NAME)) {
      String name = values.getAsString(COLUMN_PRODUCT_NAME);
      if (TextUtils.isEmpty(name)) {
        throw new IllegalArgumentException("Product requires a name");
      }
    }

    if (values.containsKey(COLUMN_PRODUCT_DESCRIPTION)) {
      String description = values.getAsString(COLUMN_PRODUCT_DESCRIPTION);
      if (TextUtils.isEmpty(description)) {
        throw new IllegalArgumentException("Product requires a description");
      }
    }

    if (values.containsKey(COLUMN_PRODUCT_PRICE)) {
      Float price = values.getAsFloat(COLUMN_PRODUCT_PRICE);
      if (price != null && price <= 0) {
        throw new IllegalArgumentException("Product requires a valid price");
      }
    }

    if (values.containsKey(COLUMN_PRODUCT_QUANTITY)) {
      int quantity = values.getAsInteger(COLUMN_PRODUCT_QUANTITY);
      if (quantity < 0) {
        throw new IllegalArgumentException("Product requires a valid quantity");
      }
    }

    if (values.containsKey(COLUMN_SUPPLIER_NAME)) {
      String supplierName = values.getAsString(COLUMN_SUPPLIER_NAME);
      if (TextUtils.isEmpty(supplierName)) {
        throw new IllegalArgumentException("Supplier name required");
      }
    }

    if (values.containsKey(COLUMN_SUPPLIER_PHONE)) {
      String supplierPhone = values.getAsString(COLUMN_SUPPLIER_PHONE);
      if (TextUtils.isEmpty(supplierPhone)) {
        throw new IllegalArgumentException("Supplier phone required");
      }
    }

    SQLiteDatabase database = productDbHelper.getWritableDatabase();
    int updatedRowId = database.update(TABLE_NAME, values, selection, selectionArgs);
    if (updatedRowId != 0) {
      getContext().getContentResolver().notifyChange(uri, null);
    }
    return updatedRowId;
  }
}
