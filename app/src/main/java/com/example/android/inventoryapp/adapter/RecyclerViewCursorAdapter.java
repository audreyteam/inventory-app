package com.example.android.inventoryapp.adapter;

import android.database.Cursor;
import android.database.DataSetObserver;
import android.support.annotation.WorkerThread;
import android.support.v7.widget.RecyclerView;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;
import com.example.android.inventoryapp.data.ProductContract.ProductEntry;

/**
 * Base Adapter for {@link Cursor} based data.
 * <p>The data can be filtered based on text criteria.</p>
 */
public abstract class RecyclerViewCursorAdapter<VH extends RecyclerView.ViewHolder> extends
    RecyclerView.Adapter<VH> implements
    Filterable {

  private Cursor mCursor;
  private boolean mDataValid;
  private int mRowIdColumn;

  private DataSetObserver mDataSetObserver;
  private FilterQueryProvider filterQueryProvider;
  private Filter cursorFilter;

  RecyclerViewCursorAdapter(Cursor cursor) {
    mCursor = cursor;
    mDataValid = cursor != null;
    mRowIdColumn = mDataValid ? mCursor.getColumnIndex("_id") : -1;
    mDataSetObserver = new NotifyingDataSetObserver();
    if (mCursor != null) {
      mCursor.registerDataSetObserver(mDataSetObserver);
    }
  }

  private Cursor getCursor() {
    return mCursor;
  }

  @Override
  public int getItemCount() {
    if (mDataValid && mCursor != null) {
      return mCursor.getCount();
    }
    return 0;
  }

  @Override
  public long getItemId(int position) {
    if (mDataValid && mCursor != null && mCursor.moveToPosition(position)) {
      return mCursor.getLong(mRowIdColumn);
    }
    return 0;
  }

  @Override
  public void setHasStableIds(boolean hasStableIds) {
    super.setHasStableIds(true);
  }

  public abstract void onBindViewHolder(VH viewHolder, Cursor cursor);

  @Override
  public void onBindViewHolder(VH viewHolder, int position) {
    if (!mDataValid) {
      throw new IllegalStateException("this should only be called when the cursor is valid");
    }
    if (!mCursor.moveToPosition(position)) {
      throw new IllegalStateException("couldn't move cursor to position " + position);
    }
    onBindViewHolder(viewHolder, mCursor);
  }

  /**
   * Swap in a new Cursor, returning the old Cursor.
   */
  public Cursor swapCursor(Cursor newCursor) {
    if (newCursor == mCursor) {
      return null;
    }
    final Cursor oldCursor = mCursor;
    if (oldCursor != null && mDataSetObserver != null) {
      oldCursor.unregisterDataSetObserver(mDataSetObserver);
    }
    mCursor = newCursor;
    if (mCursor != null) {
      if (mDataSetObserver != null) {
        mCursor.registerDataSetObserver(mDataSetObserver);
      }
      mRowIdColumn = newCursor.getColumnIndexOrThrow(ProductEntry._ID);
      mDataValid = true;
      notifyDataSetChanged();
    } else {
      mRowIdColumn = -1;
      mDataValid = false;
      notifyDataSetChanged();
      //There is no notifyDataSetInvalidated() method in RecyclerView.Adapter
    }
    return oldCursor;
  }

  public void setFilterQueryProvider(FilterQueryProvider filterQueryProvider) {
    this.filterQueryProvider = filterQueryProvider;
  }

  @WorkerThread
  private Cursor runQueryOnBackgroundThread(CharSequence constraint) {
    if (filterQueryProvider != null) {
      return filterQueryProvider.runQuery(constraint);
    }
    return mCursor;
  }

  @Override
  public Filter getFilter() {
    if (cursorFilter == null) {
      cursorFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
          Cursor cursor = runQueryOnBackgroundThread(constraint);

          FilterResults results = new FilterResults();
          if (cursor != null) {
            results.count = cursor.getCount();
            results.values = cursor;
          } else {
            results.count = 0;
            results.values = null;
          }
          return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
          Cursor oldCursor = getCursor();

          if (results.values != null && results.values != oldCursor) {
            swapCursor((Cursor) results.values);
          }
        }
      };
    }
    return cursorFilter;
  }

  /**
   * Observer that notifies when the DataSet changed.
   */
  private class NotifyingDataSetObserver extends DataSetObserver {

    @Override
    public void onChanged() {
      super.onChanged();
      mDataValid = true;
      notifyDataSetChanged();
    }

    @Override
    public void onInvalidated() {
      super.onInvalidated();
      mDataValid = false;
      notifyDataSetChanged();
    }
  }
}
