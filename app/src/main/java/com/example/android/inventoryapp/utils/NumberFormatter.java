package com.example.android.inventoryapp.utils;

import android.content.Context;
import android.util.Log;

public final class NumberFormatter {

  private static final String TAG = NumberFormatter.class.getSimpleName();

  private NumberFormatter() {
    // Shall not be constructed.
  }

  /**
   * Formats the price based on the user preference.
   */
  public static String formatPrice(Context context, float price) {
    return String.format(PreferencesUtils.getCurrencyPreference(context), price);
  }

  /**
   * Formats an integer from a string number safely.
   */
  public static int formatInt(String number) {
    int result = 0;
    try {
      result = Integer.parseInt(number);
    } catch (NumberFormatException e) {
      Log.e(TAG, "can't parse this number", e);
    }
    return result;
  }
}
