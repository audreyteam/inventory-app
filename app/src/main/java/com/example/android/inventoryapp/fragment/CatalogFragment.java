package com.example.android.inventoryapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.activity.CashRegisterActivity;
import com.example.android.inventoryapp.activity.SettingsActivity;

public abstract class CatalogFragment extends BaseBoxMenuFragment {

  protected static final int SETTINGS_REQUEST_CODE = 2;

  @BindView(R.id.recycler_view)
  RecyclerView recyclerView;

  private Unbinder unbinder;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View view = inflater.inflate(getLayoutRes(), container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  /**
   * Returns the layout resource that will be inflated by the fragment.
   */
  public abstract int getLayoutRes();

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.cash_register:
        Intent cashRegisterIntent = new Intent(getActivity(), CashRegisterActivity.class);
        startActivity(cashRegisterIntent);
        return true;
      case R.id.settings:
        Intent settingsIntent = new Intent(getActivity(), SettingsActivity.class);
        startActivityForResult(settingsIntent, SETTINGS_REQUEST_CODE);
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_store, menu);
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

}
