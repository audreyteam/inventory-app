package com.example.android.inventoryapp.data;

import com.example.android.inventoryapp.model.BoxItem;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class StoreBox {

  private static volatile StoreBox box;
  private final HashMap<Long, Integer> boxItemMap;

  private StoreBox() {
    boxItemMap = new HashMap<>();
  }

  /**
   * Gets a StoreBox instance.
   */
  public static StoreBox getInstance() {
    if (box == null) {
      // Ensures the thread safety.
      synchronized (StoreBox.class) {
        if (box == null) {
          box = new StoreBox();
        }
      }
    }
    return box;
  }

  public void addItem(BoxItem item) {
    boxItemMap.put(item.getItemId(), item.getQuantityToSell());
  }

  /**
   * Returns a {@link Collection} of product quantities.
   */
  public Collection<Integer> itemsQuantities() {
    return boxItemMap.values();
  }

  /**
   * Returns a {@link Set} of product ids.
   */
  public Set<Long> getItemsIds() {
    return boxItemMap.keySet();
  }

  public int getQuantityInBox(BoxItem item) {
    return getQuantityInBox(item.getItemId());
  }

  public int getQuantityInBox(long id) {
    if (boxItemMap.containsKey(id)) {
      return boxItemMap.get(id);
    }
    return 0;
  }

  public void removeItem(long id) {
    boxItemMap.remove(id);
  }

  public void clearItems() {
    boxItemMap.clear();
  }

  public int getSize() {
    return boxItemMap.size();
  }
}
