package com.example.android.inventoryapp.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import com.example.android.inventoryapp.R;

public class SettingsActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_settings);
  }

  public static class StorePreferenceFragment extends PreferenceFragment implements
      OnPreferenceChangeListener {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.settings);
      Preference currencyPref = findPreference(getString(R.string.settings_currency_key));
      bindPreference(currencyPref);
      Preference quantityByPref = findPreference(getString(R.string.settings_quantity_key));
      bindPreference(quantityByPref);
    }

    private void bindPreference(Preference preference) {
      preference.setOnPreferenceChangeListener(this);
      SharedPreferences preferences = PreferenceManager
          .getDefaultSharedPreferences(preference.getContext());
      String preferenceString = preferences.getString(preference.getKey(), "");
      onPreferenceChange(preference, preferenceString);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
      String stringValue = newValue.toString();
      if (preference instanceof ListPreference) {
        ListPreference listPreference = (ListPreference) preference;
        int prefIndex = listPreference.findIndexOfValue(stringValue);
        if (prefIndex >= 0) {
          CharSequence[] labels = listPreference.getEntries();
          preference.setSummary(labels[prefIndex]);
        }
      } else {
        if (Integer.parseInt(stringValue) < 1) {
          return false;
        }
        preference.setSummary(stringValue);
      }
      return true;
    }
  }
}
