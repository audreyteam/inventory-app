package com.example.android.inventoryapp.model;

public final class ProductCategory {

  private final int imageResId;
  private final String name;

  public ProductCategory(int imageResId, String name) {
    this.imageResId = imageResId;
    this.name = name;
  }

  public int getImageResId() {
    return imageResId;
  }

  public String getName() {
    return name;
  }
}
