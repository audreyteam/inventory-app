package com.example.android.inventoryapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.adapter.ItemBoxCursorAdapter.ViewHolder;
import com.example.android.inventoryapp.data.StoreBox;
import com.example.android.inventoryapp.model.BoxItem;
import com.example.android.inventoryapp.model.Product;
import com.example.android.inventoryapp.utils.NumberFormatter;

public class ItemBoxCursorAdapter extends RecyclerViewCursorAdapter<ViewHolder> {

  private final Context context;
  private OnItemBoxChangeListener listener;

  public ItemBoxCursorAdapter(Context context, Cursor cursor,
      OnItemBoxChangeListener listener) {
    super(cursor);
    this.context = context;
    this.listener = listener;
  }

  @SuppressLint("StringFormatInvalid")
  @Override
  public void onBindViewHolder(final ViewHolder viewHolder, final Cursor cursor) {
    final Product product = Product.fromCursor(cursor);
    if (product.getPhotoUri() != null) {
      // Be sure to get the image view dimensions before loading the image.
      viewHolder.productImage.post(new Runnable() {
        @Override
        public void run() {
          // Loads images in the background thread.
          new ProductImageTask(viewHolder.productImage)
              .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, product.getPhotoUri());
        }
      });
    } else {
      viewHolder.productImage.setImageBitmap(null);
    }
    viewHolder.productNameView.setText(product.getName());
    viewHolder.productDescription.setText(product.getDescription());
    viewHolder.priceView.setText(NumberFormatter.formatPrice(context, product.getPrice()));
    int quantity = StoreBox.getInstance().getQuantityInBox(product.getId());
    viewHolder.quantityToSell.setText(String.valueOf(quantity));
    float totalPrice = calculateProductPrice(quantity, product.getPrice());
    setTotalPrice(totalPrice, viewHolder.totalPrice);

    viewHolder.increaseButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        int quantityToSell = Integer.parseInt(viewHolder.quantityToSell.getText().toString());
        quantityToSell++;
        if (quantityToSell > product.getQuantity()) {
          Toast.makeText(context, context.getString(R.string.deny_sale_msg), Toast.LENGTH_SHORT)
              .show();
        } else {
          BoxItem item = new BoxItem(product.getId(), quantityToSell);
          StoreBox.getInstance().addItem(item);
          viewHolder.quantityToSell.setText(String.valueOf(quantityToSell));
          float totalPrice = calculateProductPrice(quantityToSell, product.getPrice());
          setTotalPrice(totalPrice, viewHolder.totalPrice);
          listener.onProductQuantityChanged();
        }
      }
    });

    viewHolder.decreaseButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        int quantityToSell = Integer.parseInt(viewHolder.quantityToSell.getText().toString());
        quantityToSell--;
        if (quantityToSell == 0) {
          Toast.makeText(context, context.getString(R.string.quantity_not_allowed_msg),
              Toast.LENGTH_SHORT).show();
        } else {
          BoxItem item = new BoxItem(product.getId(), quantityToSell);
          StoreBox.getInstance().addItem(item);
          viewHolder.quantityToSell.setText(String.valueOf(quantityToSell));
          float totalPrice = calculateProductPrice(quantityToSell, product.getPrice());
          setTotalPrice(totalPrice, viewHolder.totalPrice);
          listener.onProductQuantityChanged();
        }
      }
    });

    viewHolder.deleteProductButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        listener.onShowDeleteDialog(product.getId());
      }
    });
  }

  /**
   * Calculates the product price depending on the quantity selected by the user.
   */
  private float calculateProductPrice(int quantity, float price) {
    return quantity * price;
  }

  /**
   * Sets the total price.
   */
  private void setTotalPrice(float price, TextView priceView) {
    priceView.setText(NumberFormatter.formatPrice(context, price));
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_store_box, parent, false);
    ViewHolder viewHolder = new ViewHolder(itemView);
    return viewHolder;
  }

  /**
   * Listener for Item Box changes.
   */
  public interface OnItemBoxChangeListener {

    void onProductQuantityChanged();

    void onShowDeleteDialog(long id);
  }

  static class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.product_image)
    ImageView productImage;
    @BindView(R.id.product_name)
    TextView productNameView;
    @BindView(R.id.product_description)
    TextView productDescription;
    @BindView(R.id.product_price)
    TextView priceView;
    @BindView(R.id.decrease_button)
    Button decreaseButton;
    @BindView(R.id.quantity_to_sell)
    TextView quantityToSell;
    @BindView(R.id.increase_button)
    Button increaseButton;
    @BindView(R.id.total_price)
    TextView totalPrice;
    @BindView(R.id.item_layout)
    CardView itemLayout;
    @BindView(R.id.delete_product)
    ImageButton deleteProductButton;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
