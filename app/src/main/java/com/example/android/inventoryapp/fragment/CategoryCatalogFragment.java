package com.example.android.inventoryapp.fragment;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.activity.CategoryActivity;
import com.example.android.inventoryapp.adapter.ProductCategoryAdapter;
import com.example.android.inventoryapp.adapter.ProductCategoryAdapter.OnCategoryClickListener;
import com.example.android.inventoryapp.model.ProductCategory;
import java.util.ArrayList;
import java.util.List;

public class CategoryCatalogFragment extends CatalogFragment implements OnCategoryClickListener {

  public static final String EXTRA_CATEGORY_POSITION = "position";
  private static final int COLUMN_COUNT = 3;
  private static final int NO_RES_ID = -1;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), COLUMN_COUNT);
    recyclerView.setLayoutManager(layoutManager);
    ProductCategoryAdapter adapter = new ProductCategoryAdapter(getCategoryList());
    adapter.setOnCategoryClick(this);
    recyclerView.setAdapter(adapter);
    return view;
  }

  @Override
  public int getLayoutRes() {
    return R.layout.fragment_category_list;
  }

  @Override
  public void onPrepareOptionsMenu(Menu menu) {
    super.onPrepareOptionsMenu(menu);
    MenuItem menuItem = menu.findItem(R.id.search);
    menuItem.collapseActionView();
    menuItem.setVisible(false);
  }

  private List<ProductCategory> getCategoryList() {
    List<ProductCategory> categoryList = new ArrayList<>();
    Resources res = getResources();
    String[] categoryNames = res.getStringArray(R.array.category_names);
    TypedArray imageArray = res.obtainTypedArray(R.array.category_image_array);
    for (int i = 0; i < imageArray.length(); i++) {
      categoryList
          .add(new ProductCategory(imageArray.getResourceId(i, NO_RES_ID), categoryNames[i]));
    }
    imageArray.recycle();
    return categoryList;
  }

  @Override
  public void onCategoryClick(int position) {
    Intent intent = new Intent(getActivity(), CategoryActivity.class);
    intent.putExtra(EXTRA_CATEGORY_POSITION, position);
    startActivity(intent);
  }
}
