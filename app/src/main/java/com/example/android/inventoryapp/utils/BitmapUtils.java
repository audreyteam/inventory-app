package com.example.android.inventoryapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public final class BitmapUtils {

  private static final String TAG = BitmapUtils.class.getSimpleName();

  private BitmapUtils() {
    // Shall not be constructed.
  }

  public static Bitmap getBitmapFromUri(ImageView imageView, Uri uri) {
    if (uri == null || uri.toString().isEmpty() || imageView == null) {
      return null;
    }

    Context context = imageView.getContext();

    // Get the dimensions of the View
    int targetW = imageView.getWidth();
    int targetH = imageView.getHeight();

    InputStream input = null;
    try {
      input = context.getContentResolver().openInputStream(uri);

      // Get the dimensions of the bitmap
      BitmapFactory.Options bmOptions = new BitmapFactory.Options();
      bmOptions.inJustDecodeBounds = true;
      BitmapFactory.decodeStream(input, null, bmOptions);
      input.close();

      int photoW = bmOptions.outWidth;
      int photoH = bmOptions.outHeight;

      // Determine how much to scale down the image
      int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

      // Decode the image file into a Bitmap sized to fill the View
      bmOptions.inJustDecodeBounds = false;
      bmOptions.inSampleSize = scaleFactor;
      bmOptions.inPurgeable = true;

      input = context.getContentResolver().openInputStream(uri);
      Bitmap bitmap = BitmapFactory.decodeStream(input, null, bmOptions);
      Bitmap.createScaledBitmap(bitmap, targetW, targetH, false);
      input.close();
      return bitmap;

    } catch (FileNotFoundException fne) {
      Log.e(TAG, "Failed to load image.", fne);
      return null;
    } catch (Exception e) {
      Log.e(TAG, "Failed to load image.", e);
      return null;
    } finally {
      try {
        if (input != null) {
          input.close();
        }
      } catch (IOException ioe) {
        Log.e(TAG, "Failed to close the inputStream.", ioe);
      }
    }
  }
}
