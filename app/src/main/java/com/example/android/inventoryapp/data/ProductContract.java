package com.example.android.inventoryapp.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class ProductContract {

  static final String CONTENT_AUTHORITY = "com.example.android.inventoryapp";
  static final String PATH_PRODUCTS = "products";
  private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

  private ProductContract() {
    // Shall not be constructed.
  }

  public static class ProductEntry implements BaseColumns {

    public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PRODUCTS);
    /**
     * The MIME type for a list of pets.
     */
    public static final String CONTENT_LIST_TYPE =
        ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCTS;

    /**
     * The MIME type for a single pet.
     */
    public static final String CONTENT_ITEM_TYPE =
        ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCTS;

    public static final String TABLE_NAME = "products";
    public static final String _ID = BaseColumns._ID;
    public static final String COLUMN_PRODUCT_NAME = "product_name";
    public static final String SORT_BY_NAME = ProductEntry.COLUMN_PRODUCT_NAME + " ASC";
    public static final String COLUMN_PRODUCT_DESCRIPTION = "product_description";
    public static final String COLUMN_PRODUCT_PRICE = "product_price";
    public static final String COLUMN_PRODUCT_QUANTITY = "product_quantity";
    public static final String COLUMN_SUPPLIER_NAME = "supplier_name";
    public static final String COLUMN_SUPPLIER_PHONE = "supplier_phone";
    public static final String COLUMN_SUPPLIER_ADDRESS = "supplier_address";
    public static final String COLUMN_CATEGORY = "category";
    public static final String COLUMN_IMAGE = "image";
  }
}
