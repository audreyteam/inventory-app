package com.example.android.inventoryapp.fragment;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.activity.ProductEditorActivity;
import com.example.android.inventoryapp.data.ProductContract.ProductEntry;
import com.example.android.inventoryapp.data.StoreBox;
import com.example.android.inventoryapp.model.Product;
import com.example.android.inventoryapp.utils.BitmapUtils;
import com.example.android.inventoryapp.utils.NumberFormatter;
import com.example.android.inventoryapp.utils.PreferencesUtils;

public class ProductDetailsFragment extends BaseBoxMenuFragment {

  private static final int NO_RES_ID = -1;
  private static final int MIN_PRODUCT_QUANTITY = 1;

  @BindView(R.id.product_image)
  ImageView productImageView;
  @BindView(R.id.map_button)
  ImageView mapButton;
  @BindView(R.id.call_supplier)
  ImageView callSupplierView;
  @BindView(R.id.product_name)
  TextView productNameView;
  @BindView(R.id.price)
  TextView priceView;
  @BindView(R.id.category_name)
  TextView categoryNameView;
  @BindView(R.id.category_image)
  ImageView categoryImageView;
  @BindView(R.id.quantity)
  TextView quantityView;
  @BindView(R.id.description)
  TextView descriptionView;
  @BindView(R.id.decrease_button)
  Button decreaseButton;
  @BindView(R.id.quantity_to_sell)
  TextView quantityToSellView;
  @BindView(R.id.increase_button)
  Button increaseButton;
  @BindView(R.id.sale_button)
  Button saleButton;
  @BindView(R.id.supplier_name)
  TextView supplierNameView;
  @BindView(R.id.supplier_phone)
  TextView supplierPhoneView;
  @BindView(R.id.supplier_address)
  TextView supplierAddressView;
  @BindView(R.id.no_image_view)
  ImageView noImageView;
  @BindView(R.id.bottom_menu)
  View bottomMenu;

  private Unbinder unbinder;
  private Uri currentUri;
  private Product productSelected;
  private int quantityByPref;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View view = inflater.inflate(R.layout.fragment_product_details, container, false);
    unbinder = ButterKnife.bind(this, view);
    currentUri = getActivity().getIntent().getData();
    quantityByPref = NumberFormatter.formatInt(PreferencesUtils.getQuantityByPreference(getActivity()));
    return view;
  }

  @Override
  public void onStart() {
    super.onStart();
    showProductDetails();
  }

  private void showProductDetails() {
    Cursor cursor = getActivity().getContentResolver()
        .query(currentUri, null, null, null, null);
    if (cursor.moveToFirst()) {
      callSupplierView.setVisibility(View.VISIBLE);
      mapButton.setVisibility(View.VISIBLE);
      // Create the product from the cursor
      productSelected = Product.fromCursor(cursor);
      String[] categoryNames = getResources().getStringArray(R.array.category_names);
      TypedArray imageArray = getResources().obtainTypedArray(R.array.category_image_array);
      if (productSelected.getPhotoUri() != null) {
        // Be sure to get the image view dimensions before loading the image.
        productImageView.post(new Runnable() {
          @Override
          public void run() {
            Bitmap bitmap = BitmapUtils
                .getBitmapFromUri(productImageView, Uri.parse(productSelected.getPhotoUri()));
            if (productImageView != null) {
              productImageView.setImageBitmap(bitmap);
            }
          }
        });
        noImageView.setVisibility(View.GONE);
      }
      productNameView.setText(productSelected.getName());
      StringBuilder productDescription = new StringBuilder(productSelected.getDescription());
      productDescription.append("\n\n" + getString(R.string.lorem_ipsum));
      descriptionView.setText(productDescription);
      priceView.setText(NumberFormatter.formatPrice(getActivity(), productSelected.getPrice()));
      int quantity = productSelected.getQuantity() - StoreBox.getInstance()
          .getQuantityInBox(productSelected.getId());
      bottomMenu.setVisibility(quantity > 0 ? View.VISIBLE : View.GONE);
      quantityView.setText(String.valueOf(quantity));
      categoryNameView.setText(categoryNames[productSelected.getCategory()]);
      categoryImageView.setImageResource(imageArray.getResourceId(productSelected.getCategory(),
          NO_RES_ID));
      supplierNameView.setText(productSelected.getSupplierName());
      supplierPhoneView.setText(productSelected.getSupplierNumber());
      supplierAddressView.setText(productSelected.getSupplierAddress());

      if (isUnknown(productSelected.getSupplierAddress())) {
        mapButton.setVisibility(View.GONE);
      }
      quantityToSellView.setText(String.valueOf(MIN_PRODUCT_QUANTITY));
      imageArray.recycle();
    }
    cursor.close();
  }

  private void dialSupplierPhone() {
    Intent intent = new Intent(Intent.ACTION_DIAL);
    intent.setData(Uri.parse("tel:" + productSelected.getSupplierNumber()));
    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
      startActivity(intent);
    }
  }

  /**
   * Whether the text is equal to the string Unknown.
   */
  private boolean isUnknown(String text) {
    return text.equals(getString(R.string.unknown));
  }

  private void showDeleteConfirmationDialog() {
    Builder builder = new Builder(getActivity());
    builder.setMessage(R.string.delete_dialog_msg);
    builder.setPositiveButton(R.string.delete, new OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        deleteProduct();
        getActivity().finish();
        clearProductFromStoreBox();
      }
    });
    builder.setNegativeButton(R.string.cancel, new OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        // Dismiss the dialog when the user clicked the "Cancel" button.
        if (dialog != null) {
          dialog.dismiss();
        }
      }
    });
    // Create and show the AlertDialog
    AlertDialog alertDialog = builder.create();
    alertDialog.show();
  }

  private void showDenySaleDialog() {
    Builder builder = new Builder(getActivity());
    builder.setMessage(R.string.deny_sale_msg);
    builder.setNegativeButton(R.string.cancel, new OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        // User clicked the "Cancel" button, so dismiss the dialog
        if (dialog != null) {
          dialog.dismiss();
        }
      }
    });
    // Create and show the AlertDialog
    AlertDialog alertDialog = builder.create();
    alertDialog.show();
  }

  /**
   * Deletes the product from the database.
   */
  private void deleteProduct() {
    int deletedRowId = getActivity().getContentResolver().delete(currentUri, null, null);
    if (deletedRowId == 0) {
      Toast.makeText(getActivity(), getString(R.string.editor_delete_product_failed),
          Toast.LENGTH_SHORT).show();
    } else {
      Toast.makeText(getActivity(), getString(R.string.editor_delete_product_successful),
          Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_product_details, menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.edit_product:
        Intent intent = new Intent(getActivity(), ProductEditorActivity.class);
        intent.setData(currentUri);
        startActivity(intent);
        clearProductFromStoreBox();
        return true;
      case R.id.delete_product:
        showDeleteConfirmationDialog();
        return true;
      case R.id.add_to_box:
        boolean isBoxed = boxProduct(productSelected.getId());
        if (isBoxed) {
          getActivity().setResult(Activity.RESULT_OK);
        }
        showProductDetails();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  /**
   * Removes the product from the store box when the user wants to delete or edit it.
   */
  private void clearProductFromStoreBox() {
    StoreBox.getInstance().removeItem(productSelected.getId());
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @OnClick(R.id.map_button)
  public void onMapButtonClicked() {
    Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + productSelected.getSupplierAddress());
    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
    mapIntent.setPackage("com.google.android.apps.maps");
    if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
      startActivity(mapIntent);
    }
  }

  @OnClick(R.id.call_supplier)
  public void onCallSupplierClicked() {
    callSupplierView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialSupplierPhone();
      }
    });
  }

  @OnClick(R.id.decrease_button)
  public void onDecreaseButtonClicked() {
    int quantityToSell = NumberFormatter.formatInt(quantityToSellView.getText().toString());
    quantityToSell -= quantityByPref;
    if (quantityToSell < 1) {
      return;
    }
    quantityToSellView.setText(String.valueOf(quantityToSell));
  }

  @OnClick(R.id.increase_button)
  public void onIncreaseButtonClicked() {
    int quantityToSell = NumberFormatter.formatInt(quantityToSellView.getText().toString());
    quantityToSell += quantityByPref;
    int quantity = productSelected.getQuantity() - StoreBox.getInstance()
        .getQuantityInBox(productSelected.getId());
    if (quantityToSell > quantity) {
      showDenySaleDialog();
      return;
    }
    quantityToSellView.setText(String.valueOf(quantityToSell));
  }

  @OnClick(R.id.sale_button)
  public void saleProductClicked() {
    int quantityToSell = NumberFormatter.formatInt(quantityToSellView.getText().toString());
    int quantity = productSelected.getQuantity() - StoreBox.getInstance()
        .getQuantityInBox(productSelected.getId());
    if (quantityToSell > quantity) {
      showDenySaleDialog();
    } else {
      int newQuantityToSell = productSelected.getQuantity() - quantityToSell;
      ContentValues values = new ContentValues();
      values.put(ProductEntry.COLUMN_PRODUCT_QUANTITY, newQuantityToSell);
      int rowUpdated = getActivity().getContentResolver().update(currentUri, values, null, null);
      if (rowUpdated != 0) {
        float cashEarned = quantityToSell * productSelected.getPrice();
        PreferencesUtils.putCash(getActivity(), cashEarned);
        clearProductFromStoreBox();
        showProductDetails();
        onInvalidateOptionsMenu();
        Toast.makeText(getActivity(),
            String.format(getString(R.string.sale_successful), NumberFormatter
                .formatPrice(getActivity(), cashEarned)), Toast.LENGTH_SHORT)
            .show();
      } else {
        Toast.makeText(getActivity(), getString(R.string.sale_failed), Toast.LENGTH_SHORT).show();
      }
    }
  }
}
