package com.example.android.inventoryapp.fragment;

import static android.app.Activity.RESULT_OK;

import android.Manifest.permission;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.data.ProductContract.ProductEntry;
import com.example.android.inventoryapp.model.Product;
import com.example.android.inventoryapp.model.Product.ProductBuilder;
import com.example.android.inventoryapp.utils.BitmapUtils;
import com.example.android.inventoryapp.utils.PreferencesUtils;

public class ProductEditorFragment extends Fragment {

  private static final String TAG = ProductEditorFragment.class.getSimpleName();
  private static final String PICK_IMAGE_TYPE = "image/*";
  private static final int GET_IMAGE_REQUEST = 1;

  @BindView(R.id.photo)
  ImageView photoView;
  @BindView(R.id.add_photo)
  ImageView addPhotoView;
  @BindView(R.id.delete_photo)
  ImageView deletePhotoView;
  @BindView(R.id.product_name)
  TextInputEditText productNameView;
  @BindView(R.id.spinner_category)
  AppCompatSpinner spinnerCategory;
  @BindView(R.id.description)
  TextInputEditText descriptionView;
  @BindView(R.id.price)
  TextInputEditText priceView;
  @BindView(R.id.quantity)
  TextInputEditText quantityView;
  @BindView(R.id.supplier_name)
  TextInputEditText supplierNameView;
  @BindView(R.id.supplier_phone)
  TextInputEditText supplierPhoneView;
  @BindView(R.id.supplier_address)
  TextInputEditText supplierAddressView;
  private Unbinder unbinder;
  private Context context;
  private Uri currentUri;
  private String photoUriString;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View view = inflater.inflate(R.layout.fragment_editor, container, false);
    context = getActivity();
    unbinder = ButterKnife.bind(this, view);
    // This uri is used when the user needs to update a product.
    currentUri = getActivity().getIntent().getData();
    if (currentUri != null) {
      getActivity().setTitle(getString(R.string.editor_fragment_title_edit_product));
      loadProductData();
    } else {
      getActivity().setTitle(getString(R.string.editor_fragment_title_new_product));
    }

    photoView.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        if (ContextCompat.checkSelfPermission(getActivity(), permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
          // Request permission to pick photos from the gallery.
          ActivityCompat.requestPermissions(getActivity(),
              new String[]{permission.READ_EXTERNAL_STORAGE},
              GET_IMAGE_REQUEST);
        } else {
          pickImage();
        }
      }
    });
    addPhotoView.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        photoView.performClick();
      }
    });

    deletePhotoView.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        photoView.setImageBitmap(null);
        setPhotoViewState(false);
      }
    });
    return view;
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == GET_IMAGE_REQUEST && resultCode == RESULT_OK) {
      if (data != null && data.getData() != null) {
        // Get the photo uri after picking it from the gallery.
        Uri photoUri = data.getData();
        if (Build.VERSION.SDK_INT >= VERSION_CODES.KITKAT) {
          final int takeFlags = data.getFlags() & (Intent.FLAG_GRANT_READ_URI_PERMISSION
              | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
          try {
            getActivity().getContentResolver().takePersistableUriPermission(photoUri, takeFlags);
          } catch (SecurityException e) {
            e.printStackTrace();
          }
        }
        photoUriString = photoUri.toString();
        Bitmap bitmap = BitmapUtils.getBitmapFromUri(photoView, photoUri);
        photoView.setImageBitmap(bitmap);
        setPhotoViewState(true);
      }
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    switch (requestCode) {
      case GET_IMAGE_REQUEST:
        // If the request is granted, the result arrays are not empty
        // so the user is allowed to pick images from the gallery
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          pickImage();
        } else {
          showDenyPermissionDialog();
        }
        break;
    }
  }

  /**
   * Shows a dialog when the user denies the permission.
   */
  private void showDenyPermissionDialog() {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setMessage(R.string.refuse_permission_msg);
    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        // User clicked the "Cancel" button, so dismiss the dialog
        if (dialog != null) {
          dialog.dismiss();
        }
      }
    });
    // Create and show the AlertDialog
    AlertDialog alertDialog = builder.create();
    alertDialog.show();
  }

  /**
   * Shows the delete icon and hides the add photo icon if an image is picked from the gallery.
   */
  private void setPhotoViewState(boolean isVisible) {
    addPhotoView.setVisibility(isVisible ? View.GONE : View.VISIBLE);
    deletePhotoView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_save, menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.save:
        Product product = getProduct();
        if (product != null) {
          saveProduct(product);
          if (PreferencesUtils.shouldClearEditor(getActivity())) {
            clearProductData();
          }
          if (currentUri != null) {
            getActivity().finish();
          }
        } else {
          Log.e(TAG, "Can't insert a null product");
        }
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  /**
   * Saves the product data inserted in the editor.
   */
  private void saveProduct(Product product) {
    ContentValues values = new ContentValues();
    values.put(ProductEntry.COLUMN_PRODUCT_NAME, product.getName());
    values.put(ProductEntry.COLUMN_PRODUCT_DESCRIPTION, product.getDescription());
    values.put(ProductEntry.COLUMN_PRODUCT_PRICE, product.getPrice());
    values.put(ProductEntry.COLUMN_PRODUCT_QUANTITY, product.getQuantity());
    values.put(ProductEntry.COLUMN_CATEGORY, product.getCategory());
    values.put(ProductEntry.COLUMN_IMAGE, product.getPhotoUri());
    values.put(ProductEntry.COLUMN_SUPPLIER_NAME, product.getSupplierName());
    values.put(ProductEntry.COLUMN_SUPPLIER_PHONE, product.getSupplierNumber());
    if (!isInvalidTextEntry(product.getSupplierAddress())) {
      values.put(ProductEntry.COLUMN_SUPPLIER_ADDRESS, product.getSupplierAddress());
    }
    if (currentUri != null) {
      int rowUpdated = getActivity().getContentResolver().update(currentUri, values, null, null);
      if (rowUpdated == 0) {
        Toast.makeText(context, getString(R.string.editor_update_product_failed),
            Toast.LENGTH_SHORT).show();
      } else {
        Toast.makeText(context, getString(R.string.editor_update_product_successful),
            Toast.LENGTH_SHORT).show();
      }
    } else {
      Uri rowUri = context.getContentResolver().insert(ProductEntry.CONTENT_URI, values);
      if (rowUri == null) {
        Toast.makeText(context, getString(R.string.error_insert_toast), Toast.LENGTH_SHORT).show();
      } else {
        Toast.makeText(context, getString(R.string.success_insert_toast),
            Toast.LENGTH_SHORT)
            .show();
      }
    }
  }

  private Product getProduct() {
    // check the name validation
    String name = productNameView.getText().toString().trim();
    if (isInvalidTextEntry(name)) {
      productNameView.setError(getString(R.string.required_text_name));
      return null;
    }
    // check the description validation
    String description = descriptionView.getText().toString().trim();
    if (isInvalidTextEntry(description)) {
      descriptionView.setError(getString(R.string.required_text_description));
      return null;
    }
    // check the price validation
    String price = priceView.getText().toString().trim();
    if (isInvalidTextEntry(price)) {
      priceView.setError(getString(R.string.required_text_price));
      return null;
    }
    float priceConverted = Float.parseFloat(price);
    if (isInvalidNumberEntry(priceConverted)) {
      priceView.setError(getString(R.string.invalid_text_price));
      return null;
    }
    // check the quantity validation
    String quantity = quantityView.getText().toString().trim();
    if (isInvalidTextEntry(quantity)) {
      quantityView.setError(getString(R.string.required_text_quantity));
      return null;
    }
    int quantityConverted = Integer.parseInt(quantity);
    if (isInvalidNumberEntry(quantityConverted)) {
      quantityView.setError(getString(R.string.invalid_text_quantity));
      return null;
    }
    // check the supplier name validation
    String supplierName = supplierNameView.getText().toString().trim();
    if (isInvalidTextEntry(supplierName)) {
      supplierNameView.setError(getString(R.string.required_supplier_name));
      return null;
    }
    // check the supplier phone validation
    String supplierPhone = supplierPhoneView.getText().toString().trim();
    if (isInvalidTextEntry(supplierPhone)) {
      supplierPhoneView.setError(getString(R.string.required_supplier_phone));
      return null;
    }
    String supplierAddress = supplierAddressView.getText().toString().trim();

    int category = spinnerCategory.getSelectedItemPosition();

    // Create the product
    Product product = new ProductBuilder()
        .setName(name)
        .setDescription(description)
        .setPrice(priceConverted)
        .setQuantity(quantityConverted)
        .setSupplierName(supplierName)
        .setSupplierNumber(supplierPhone)
        .setSupplierAddress(supplierAddress)
        .setCategory(category)
        .setPhotoUri(photoUriString)
        .create();
    return product;
  }

  /**
   * Loads the product data when the user needs to update the product.
   */
  private void loadProductData() {
    Cursor cursor = getActivity().getContentResolver()
        .query(currentUri, null, null, null, null);
    if (cursor.moveToFirst()) {
      final Product productToLoad = Product.fromCursor(cursor);
      // Update the views on the screen with the values from the database
      productNameView.setText(productToLoad.getName());
      descriptionView.setText(productToLoad.getDescription());
      priceView.setText(String.valueOf(productToLoad.getPrice()));
      quantityView.setText(String.valueOf(productToLoad.getQuantity()));
      spinnerCategory.setSelection(productToLoad.getCategory());
      if (productToLoad.getPhotoUri() != null) {
        // By using post, we ensure that the view dimension are known when fetching the bitmap.
        // Without that, the width and height will be equal to 0 and we will not be able to determine
        // the image size for resizing. Also, as the post can happen after the view destruction,
        // we need to check the nullity before setting the bitmap.
        photoView.post(new Runnable() {
          @Override
          public void run() {
            Bitmap bitmap = BitmapUtils
                .getBitmapFromUri(photoView, Uri.parse(productToLoad.getPhotoUri()));
            if (photoView != null) {
              photoView.setImageBitmap(bitmap);
            }
          }
        });
        setPhotoViewState(true);
      } else {
        setPhotoViewState(false);
      }
      supplierNameView.setText(productToLoad.getSupplierName());
      supplierPhoneView.setText(productToLoad.getSupplierNumber());
      if (isValidText(productToLoad.getSupplierAddress())) {
        supplierAddressView.setText(productToLoad.getSupplierAddress());
      }
    }
    cursor.close();
  }

  private boolean isValidText(String text) {
    return !text.equals(getString(R.string.unknown));
  }

  private void clearProductData() {
    productNameView.setText("");
    priceView.setText("");
    quantityView.setText("");
    descriptionView.setText("");
    supplierNameView.setText("");
    supplierPhoneView.setText("");
    supplierAddressView.setText("");
    photoView.setImageBitmap(null);
    spinnerCategory.setSelection(0);
  }

  private void pickImage() {
    Intent intent;
    if (Build.VERSION.SDK_INT < VERSION_CODES.KITKAT) {
      intent = new Intent(Intent.ACTION_GET_CONTENT);
    } else {
      intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
      intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
      intent.addCategory(Intent.CATEGORY_OPENABLE);
    }
    intent.setType(PICK_IMAGE_TYPE);
    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
      startActivityForResult(Intent.createChooser(intent, "Select Picture"), GET_IMAGE_REQUEST);
    }
  }

  private boolean isInvalidTextEntry(String entry) {
    return TextUtils.isEmpty(entry);
  }

  private boolean isInvalidNumberEntry(float entry) {
    return entry <= 0;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }
}
