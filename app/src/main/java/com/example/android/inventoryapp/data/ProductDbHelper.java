package com.example.android.inventoryapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.android.inventoryapp.data.ProductContract.ProductEntry;

public class ProductDbHelper extends SQLiteOpenHelper {

  private static final String DATABASE_NAME = "store.db";
  private static final int DATABASE_VERSION = 1;

  private static final String SUPPLIER_DEFAULT_VALUE = " Unknown";
  private static final String COMA_SEP = ", ";
  private static final String SQL_CREATE_ENTRIES =
      "CREATE TABLE " + ProductEntry.TABLE_NAME + "("
          + ProductEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
          + ProductEntry.COLUMN_PRODUCT_NAME + " TEXT NOT NULL,"
          + ProductEntry.COLUMN_PRODUCT_DESCRIPTION + " TEXT NOT NULL,"
          + ProductEntry.COLUMN_PRODUCT_PRICE + " REAL NOT NULL,"
          + ProductEntry.COLUMN_PRODUCT_QUANTITY + " INTEGER NOT NULL,"
          + ProductEntry.COLUMN_IMAGE + " TEXT,"
          + ProductEntry.COLUMN_CATEGORY + " INTEGER NOT NULL,"
          + ProductEntry.COLUMN_SUPPLIER_NAME + " TEXT NOT NULL,"
          + ProductEntry.COLUMN_SUPPLIER_PHONE + " TEXT NOT NULL,"
          + ProductEntry.COLUMN_SUPPLIER_ADDRESS + " TEXT NOT NULL DEFAULT" + SUPPLIER_DEFAULT_VALUE
          + ")";


  ProductDbHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(SQL_CREATE_ENTRIES);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

  }
}
