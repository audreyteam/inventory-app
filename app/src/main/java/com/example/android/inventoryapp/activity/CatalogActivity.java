package com.example.android.inventoryapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.fragment.CategoryCatalogFragment;
import com.example.android.inventoryapp.fragment.ProductCatalogFragment;

public class CatalogActivity extends AppCompatActivity {

  @BindView(R.id.tab_layout)
  TabLayout tabLayout;
  @BindView(R.id.view_pager)
  ViewPager viewPager;
  @BindView(R.id.floating_button)
  FloatingActionButton floatingButton;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_catalog);
    ButterKnife.bind(this);
    final CatalogAdapter viewPagerAdapter = new CatalogAdapter(getSupportFragmentManager());
    viewPager.setAdapter(viewPagerAdapter);
    tabLayout.setupWithViewPager(viewPager);
    viewPager.addOnPageChangeListener(new OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

      }

      @Override
      public void onPageSelected(int position) {
        switch (position) {
          case 0:
            floatingButton.show();
            break;
          case 1:
            floatingButton.hide();
            break;
        }
      }

      @Override
      public void onPageScrollStateChanged(int state) {

      }
    });
  }

  @OnClick(R.id.floating_button)
  public void onFloattingButtonClicked() {
    Intent intent = new Intent(CatalogActivity.this, ProductEditorActivity.class);
    startActivity(intent);
  }

  public interface OnInvalidateOptionsMenuListener {

    void onInvalidateOptionsMenu();
  }

  private class CatalogAdapter extends FragmentPagerAdapter {

    private static final int PAGE_COUNT = 2;

    public CatalogAdapter(FragmentManager fm) {
      super(fm);
    }

    @Override
    public Fragment getItem(int position) {
      switch (position) {
        case 0:
          return new ProductCatalogFragment();
        case 1:
          return new CategoryCatalogFragment();
        default:
          return null;
      }
    }

    @Override
    public CharSequence getPageTitle(int position) {
      switch (position) {
        case 0:
          return getString(R.string.products_page_title);
        case 1:
          return getString(R.string.categories_page_title);
        default:
          return null;
      }
    }

    @Override
    public int getCount() {
      return PAGE_COUNT;
    }
  }
}
