package com.example.android.inventoryapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.example.android.inventoryapp.R;

public final class PreferencesUtils {

  private static final String CASH_PREFERENCE_KEY = "cash_preference_key";

  private PreferencesUtils() {
    // Shall not be constructed.
  }

  /**
   * Returns the currency preference that is use to display prices. */
  public static String getCurrencyPreference(Context context) {
    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    String currencyPref = sharedPrefs.getString(context.getString(R.string.settings_currency_key),
        context.getString(R.string.settings_currency_default));
    return currencyPref;
  }

  /**
   * Returns the incremental quantity preference when adding product to the box. */
  public static String getQuantityByPreference(Context context) {
    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    String quantityByPref = sharedPrefs.getString(context.getString(R.string.settings_quantity_key),
        context.getString(R.string.settings_quantity_default));
    return quantityByPref;
  }

  /**
   * Whether to clear the editor when user saves the product.
   */
  public static boolean shouldClearEditor(Context context) {
    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    boolean shouldClearPref = sharedPrefs
        .getBoolean(context.getString(R.string.clear_editor_fields_key), true);
    return shouldClearPref;
  }

  /**
   * Saves the cash collected by the user by selling product into the preference.
   */
  public static void putCash(Context context, float cash) {
    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    SharedPreferences.Editor editor = sharedPrefs.edit();
    cash += getCash(context);
    editor.putFloat(CASH_PREFERENCE_KEY, cash);
    editor.apply();
  }

  /**
   * Gets the cash collected by the user from the preference.
   */
  public static float getCash(Context context) {
    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    return sharedPrefs.getFloat(CASH_PREFERENCE_KEY, 0);
  }
}
