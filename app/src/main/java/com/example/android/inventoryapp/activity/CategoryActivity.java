package com.example.android.inventoryapp.activity;

import static com.example.android.inventoryapp.fragment.ProductCatalogFragment.CATEGORIES_LOADER_ID;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.fragment.ProductCatalogFragment;

public class CategoryActivity extends AppCompatActivity {

  private ProductCatalogFragment fragment;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_container);
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    fragment = new ProductCatalogFragment();
    transaction.replace(R.id.fragment_container, fragment).commit();
  }

  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    // We need to explicitly restart the loader because the activity launch mode is single top.
    getSupportLoaderManager().restartLoader(CATEGORIES_LOADER_ID, null, fragment);
  }
}
