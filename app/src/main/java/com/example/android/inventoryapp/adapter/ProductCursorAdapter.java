package com.example.android.inventoryapp.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.adapter.ProductCursorAdapter.ViewHolder;
import com.example.android.inventoryapp.data.StoreBox;
import com.example.android.inventoryapp.model.Product;
import com.example.android.inventoryapp.utils.NumberFormatter;

public class ProductCursorAdapter extends RecyclerViewCursorAdapter<ViewHolder> {

  private static final int MIN_QUANTITY_FOR_LEVEL_0_STOCK_ALERT = 20;
  private static final int MIN_QUANTITY_IN_STOCK = 1;
  private static final int MIN_QUANTITY_FOR_LEVEL_2_STOCK_ALERT = 9;

  private final Context context;
  private OnProductClickListener listener;

  public ProductCursorAdapter(Context context, Cursor cursor) {
    super(cursor);
    this.context = context;
  }

  @SuppressLint("StringFormatInvalid")
  @Override
  public void onBindViewHolder(final ViewHolder viewHolder, Cursor cursor) {
    final Product product = Product.fromCursor(cursor);
    if (product.getPhotoUri() != null) {
      // Be sure to get the image view dimensions before loading the image.
      viewHolder.productImage.post(new Runnable() {
        @Override
        public void run() {
          // Loads images in the background thread.
          new ProductImageTask(viewHolder.productImage)
              .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, product.getPhotoUri());
        }
      });
    } else {
      viewHolder.productImage.setImageBitmap(null);
    }
    viewHolder.productNameView.setText(product.getName());
    viewHolder.productDescription.setText(product.getDescription());
    viewHolder.priceView.setText(NumberFormatter.formatPrice(context, product.getPrice()));
    int quantity = product.getQuantity() - StoreBox.getInstance().getQuantityInBox(product.getId());
    // Show a stock alert badge depending on the product quantity remained.
    if (quantity >= MIN_QUANTITY_IN_STOCK) {
      viewHolder.inStockContainer.setVisibility(View.VISIBLE);
      viewHolder.outStockContainer.setVisibility(View.INVISIBLE);
      viewHolder.inStockView.setVisibility(View.VISIBLE);
      viewHolder.inStockView
          .setText(context.getString(R.string.in_stock_text_for_item, quantity));
      if (quantity > MIN_QUANTITY_FOR_LEVEL_0_STOCK_ALERT) {
        viewHolder.inStockView
            .setBackgroundResource(R.drawable.stock_level0_alert_badge_drawable);
      } else if (quantity > MIN_QUANTITY_FOR_LEVEL_2_STOCK_ALERT) {
        viewHolder.inStockView
            .setBackgroundResource(R.drawable.stock_level1_alert_badge_drawable);
      } else {
        viewHolder.inStockView
            .setBackgroundResource(R.drawable.stock_level2_alert_badge_drawable);
      }
    } else {
      viewHolder.inStockContainer.setVisibility(View.INVISIBLE);
      viewHolder.outStockContainer.setVisibility(View.VISIBLE);
    }

    viewHolder.addToBoxButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        listener.onBoxIconClick(product.getId(), viewHolder.getAdapterPosition());
      }
    });

    viewHolder.itemContainer.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        listener.onProductClick(product.getId(), viewHolder.getAdapterPosition());
      }
    });
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_product, parent, false);
    ViewHolder viewHolder = new ViewHolder(itemView);
    return viewHolder;
  }

  public void setOnProductClick(OnProductClickListener listener) {
    this.listener = listener;
  }

  /**
   * Listener for Product click events
   */
  public interface OnProductClickListener {

    void onProductClick(long productId, int position);

    void onBoxIconClick(long productId, int position);
  }

  static class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.product_image)
    ImageView productImage;
    @BindView(R.id.product_name)
    TextView productNameView;
    @BindView(R.id.product_description)
    TextView productDescription;
    @BindView(R.id.product_price)
    TextView priceView;
    @BindView(R.id.in_stock_view)
    TextView inStockView;
    @BindView(R.id.add_to_box_button)
    ImageView addToBoxButton;
    @BindView(R.id.in_stock_container)
    RelativeLayout inStockContainer;
    @BindView(R.id.out_stock_view)
    TextView outStockView;
    @BindView(R.id.out_stock_container)
    RelativeLayout outStockContainer;
    @BindView(R.id.item_layout)
    RelativeLayout itemContainer;

    ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
