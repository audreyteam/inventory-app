package com.example.android.inventoryapp.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.android.inventoryapp.R;
import com.example.android.inventoryapp.utils.NumberFormatter;
import com.example.android.inventoryapp.utils.PreferencesUtils;

public class CashRegisterActivity extends AppCompatActivity {

  @BindView(R.id.cash_text)
  TextView cashText;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cash_register);
    ButterKnife.bind(this);
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
    }
    // Display what the user has earned during sales.
    float cash = PreferencesUtils.getCash(this);
    cashText.setText(NumberFormatter.formatPrice(this, cash));
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      finish();
    }
    return super.onOptionsItemSelected(item);
  }
}
